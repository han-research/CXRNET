# CXR-Net: A Multitask Deep Learning Network for Explainable and Accurate Diagnosis of COVID-19 pneumonia with Chest X-ray Images [Link](https://ieeexplore.ieee.org/document/9944138)

By Xin Zhang, Liangxiu Han*, Tam Sobeih, Lianghao Han, Nina Dempsey,  Symeon Lechareas, Ascanio Tridente, Haoming Chen, Stephen White, Daoqiang Zhang

Cite as:

Zhang, Xin, Liangxiu Han, Tam Sobeih, Lianghao Han, Nina Dempsey, Symeon Lechareas, Ascanio Tridente, Haoming Chen, Stephen White, and Daoqiang Zhang. ‘CXR-Net: A Multitask Deep Learning Network for Explainable and Accurate Diagnosis of COVID-19 Pneumonia from Chest X-Ray Images’. IEEE Journal of Biomedical and Health Informatics, 2022, 1–15. https://doi.org/10.1109/JBHI.2022.3220813.



## Introduction

Accurate and rapid detection of COVID-19 pneumonia is crucial for optimal patient treatment. Chest X-Ray (CXR) is the first line imaging test for COVID-19 pneumonia diagnosis as it is fast, cheap and easily accessible. Currently, many deep learning (DL) models have been proposed to detect COVID-19 pneumonia using CXR images. Unfortunately, these deep classifiers lack the transparency in interpreting findings, which may limit their applications in clinical practice. The existing explanation methods used produce either too noisy or imprecise results, and hence are unsuitable for diagnostic purposes. In this work, we propose a novel explainable CXR deep neural Network (CXRNet) for accurate COVID-19 pneumonia detection with an enhanced pixel-level visual explanation from CXR images. An Encoder-Decoder-Encoder architecture is proposed in which an extra encoder is added after the decoder to ensure the model can be trained with category samples. The method has been evaluated on real world CXR datasets from both public and private sources, including healthy, bacterial pneumonia, viral pneumonia and COVID-19 pneumonia cases. The results demonstrate that the proposed method can achieve a satisfactory accuracy and provide fine-resolution activation maps for visual explanation in the lung disease detection. The Average Accuracy, the Sensitivity, Specificity, PPV and F1-score of COVID-19 pneumonia reached 0.992, 0.998, 0.985 and 0.989, respectively. Compared to current state-of-the-art visual explanation methods, the proposed method can provide more detailed high resolution visual explanation for the classification decision and can be deployed in various computing environments, including cloud, CPU and GPU environments. It has a great potential to be used in clinical practice for COVID-19 pneumonia diagnosis. 

![Flowchat](./imgs/figure1.png)



## Getting started


## Authors and acknowledgment

## License

## Project status
In this repository we release models from the paper. 


